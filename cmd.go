package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	f := flag.String("f", "./", "path to files")
	p := flag.Int("p", 8080, "port")
	flag.Parse()

	http.Handle("/", http.FileServer(http.Dir(*f)))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", *p), nil))
}
